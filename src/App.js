import React, { Component } from 'react';
import {Navbar, NavbarBrand} from 'reactstrap';
import logo from './logo.svg';
import './App.css';
import Menu from './components/Menucomponent';
import {DISHES} from './sharefolder/dishes';


class App extends Component{
  constructor(props){
    super(props);
    this.state= {
      dishes:DISHES
    };
  }
render(){
  return(
<div>
<Navbar dark color="danger ">
  <div className="container">
    <NavbarBrand href="/">Food Store</NavbarBrand>
    </div>
  </Navbar>
<Menu dishes={this.state.dishes}/>
</div>




  );
}

}

export default App;
